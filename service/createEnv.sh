source .config;

echo 'PROJECTNAME='$PROJECTNAME > .env;
echo 'MARK='"$(date +%s)">> .env;
echo 'DEVHOST='$DEVHOST >> .env;
echo 'MYSQL_ROOT_PASSWORD='$MYSQL_ROOT_PASSWORD >> .env;
echo 'MYSQL_USER='$MYSQL_USER >> .env;
echo 'MYSQL_PASSWORD='$MYSQL_PASSWORD >> .env;
echo 'RABBITMQ_DEFAULT_USER='$RABBITMQ_DEFAULT_USER >> .env;
echo 'RABBITMQ_DEFAULT_PASS='$RABBITMQ_DEFAULT_PASS >> .env;
