source .config;
mkdir ./components/nginx/conf.d -p;

echo 'server {
	listen 80;

	  set $yii_bootstrap "index.php";

    index $yii_bootstrap;
    server_name '$PROJECTNAME';
    error_log  /var/log/nginx/'$PROJECTNAME'-error.log;
    access_log /var/log/nginx/'$PROJECTNAME'-access.log;
    root /var/www/http/website/web;

    location / {
        index  index.html $yii_bootstrap;
        try_files $uri $uri/ /$yii_bootstrap?$args;
    }

	location ~* \.(jpg|jpeg|gif|css|png|js|ico|html)$ {
		access_log off;
		expires max;
		log_not_found off;
	}

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php-fpm:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}' > ./components/nginx/conf.d/default.conf;
