source .config;

echo "zend_extension=xdebug.so

xdebug.mode=debug
xdebug.start_with_request=yes
xdebug.client_host=$DEVHOST
xdebug.client_port=$DEBUGPORT_0
xdebug.remote_handler=dbgp
xdebug.log=/var/log/xdebug.log
xdebug.xdebug.discover_client_host=on
" > ./components/php-fpm/xdebug.ini;

echo "zend_extension=xdebug.so

xdebug.mode=debug
xdebug.start_with_request=yes
xdebug.client_host=$DEVHOST
xdebug.client_port=$DEBUGPORT_1
xdebug.remote_handler=dbgp
xdebug.log=/var/log/xdebug.log
xdebug.xdebug.discover_client_host=on
" > ./components/supervisor/xdebug.ini;
