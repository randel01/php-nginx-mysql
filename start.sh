cp .config ./website/
. ./service/createEnv.sh
. ./service/createNginxConfig.sh
. ./service/createXdebugConfig.sh

docker-compose stop
docker-compose up --build --no-start
docker-compose start

