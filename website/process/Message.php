<?php

class Message
{
    private $message;

    public function __construct($message)
    {
        unset($message->class, $message->method);

        $this->message = $message;
    }

    public function printMessage(): void
    {
        echo __METHOD__.PHP_EOL;

        echo print_r($this->message, true).PHP_EOL;
    }
}
