<?php

class Config
{
    public const HOST = 'rabbitmq';
    public const PORT = 5672;

    public const USERNAME = 'user';
    public const PASSWORD = 'password';

    public const EXCHANGE = 'TEST.Messages.1.0';
    public const QUEUE = 'TEST.Messages';
    public const QUEUE1 = 'TEST.Messages.1';
    public const ROUTE = 'TEST.Route';
    public const VHOST = 'test';
}
