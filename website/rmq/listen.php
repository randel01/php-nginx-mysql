<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/Config.php';
require_once __DIR__.'/../process/Message.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection(Config::HOST, Config::PORT, Config::USERNAME, Config::PASSWORD);

$channel = $connection->channel();

$channel->queue_declare(
    Config::QUEUE,
    false,
    true,
    false,
    false
);

$channel->basic_consume(
    Config::QUEUE,
    null,
    false,
    true,
    false,
    false,
    'callBack'

);

while (count($channel->callbacks)) {
    try {
        $channel->wait();
    } catch (ErrorException $e) {
    }
}

function callBack(AMQPMessage $message)
{
    $message = json_decode($message->body, false);

    if (method_exists($message->class, $message->method)) {

        echo __METHOD__.PHP_EOL;
        print_r($message);

        (new $message->class($message))->{$message->method};
    }
}
