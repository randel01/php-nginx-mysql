<?php
ini_set('max_execution_time', 900);
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/Config.php';
require_once __DIR__.'/../process/Message.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection(Config::HOST, Config::PORT, Config::USERNAME, Config::PASSWORD);

$channel = $connection->channel();

$channel->queue_declare(
    Config::QUEUE,
    false,
    true,
    false,
    false
);

$messageBody = [
    'Time' => date('d-m-Y H:i:s'),
    'Microtime' => microtime(true),
];

$messageIdent = [
    'class' => Message::class,
    'method' => 'printMessage',
];

$message = new AMQPMessage(
    json_encode(array_merge($messageBody, $messageIdent)),
    ['delivery_mode' => 2]
);

for ($i = 0; $i < 100; $i++) {
    $channel->basic_publish(
        $message,
        Config::EXCHANGE,
        Config::ROUTE,
        Config::QUEUE
    );
}
$channel->close();
try {
    $connection->close();
} catch (Exception $e) {
}

