<?php /** @noinspection PhpComposerExtensionStubsInspection */

use Predis\Client;

require_once __DIR__.'/../vendor/autoload.php';

$redis = new Client(
    [
        'host' => 'redis',
    ]
);

if (!$redis->exists('projectParameters')) {
    $projectParameters = parse_ini_file(__DIR__.'/../.config');
    $redis->set('projectParameters', json_encode($projectParameters));
} else {
    $projectParameters = json_decode($redis->get('projectParameters'), true);
}

$dsn = "mysql:host=mysql;dbname={$projectParameters['PROJECTNAME']}";
$user = $projectParameters['MYSQL_USER'];
$password = $projectParameters['MYSQL_PASSWORD'];

try {
    $dbConnect = new PDO($dsn, $user, $password);
    if (!$redis->exists('databaseCreated') || !$redis->get('databaseCreated')) {
        $redis->set('databaseCreated',  $dbConnect->exec("CREATE DATABASE IF NOT EXISTS {$projectParameters['PROJECTNAME']} COLLATE utf8_bin"));
    }
} catch (PDOException $e) {
    echo 'Подключение не удалось: '.$e->getMessage();
}

phpinfo();
